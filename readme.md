In this application, only the basic machinery is implemented: simple form with template and requests to BaseCamp API using preliminary manually retrieved access token.

Taking into account that the full authentication process will require some additional work, it can be discussed and implemented if it will be necessary.

Access token was retrieved by sending request with cURL to the token-exchange-endpoint:

https://launchpad.37signals.com/authorization/token

This token has big lifetime by default so it should be enough for test task purposes :)

Next, some additional IDs were retrieved manually with the similar way:

- Project ID:
https://3.basecampapi.com/3472788/projects.json
- Todo-set ID:
https://3.basecampapi.com/3472788/projects/1190341.json
- Todo-list ID:
https://3.basecampapi.com/3472788/buckets/1190341/todosets/177952765/todolists.json

This sequence of requests is also may be implemented on python-side.

Regarding to the part which is implemented in the scope of this app, a couple of requests are initiating on form submition:
- First, attached file is uploading to the basecamp server
- Then, using attachment-id received on previous step, a new todo item is creating with the submitted file in its description.

Considering the main part of application is interaction with the 3-rd party API without complex logic, unittests were not implemented.
However, for this app it's possible to implement simple smoke-tests using mock-objects.
 