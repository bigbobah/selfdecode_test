from django.shortcuts import render

import requests

from . import forms


ACCESS_TOKEN = 'BAhbB0kiAbl7ImNsaWVudF9pZCI6ImQ3YjA4N2IxOWZiZmQwZDZjMjBmY2RlMDQ5MjFjY2U2MjgzNDlkMTciLCJleHBpcmVzX2F0IjoiMjAxNy0xMC0xM1QxNjozODo1M1oiLCJ1c2VyX2lkcyI6WzMzNzI3MjU1LDMzNzMyMTQ4XSwidmVyc2lvbiI6MSwiYXBpX2RlYWRib2x0IjoiZjU0MWIxYmY4OTkyYjA1Zjk1ODZiZTFkY2FiN2I5NTgifQY6BkVUSXU6CVRpbWUNsGUdwBWjUpsJOg1uYW5vX251bWkCXgM6DW5hbm9fZGVuaQY6DXN1Ym1pY3JvIgeGIDoJem9uZUkiCFVUQwY7AEY=--9ac9af7fbae3ac41b6361d8ba6fc56e65c080e93'

headers = {
    'Authorization': 'Bearer {}'.format(ACCESS_TOKEN),
    'User-Agent': 'MyApp (yourname@example.com)',
}
base_url = 'https://3.basecampapi.com/3472788/'


def upload_file(file):
    url = '{}attachments.json?name={}'.format(base_url, file.name)
    res = requests.post(url, data=file.read(), headers=headers)
    return res.json()['attachable_sgid']


def audio_file_view(request):
    if request.method == 'POST':
        form = forms.AudioFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_attachment_id = upload_file(form.cleaned_data['file'])

            url = '{base_url}buckets/{project_id}/todolists/{todolist_id}/todos.json'.format(
                base_url=base_url, project_id='1190341', todolist_id='657088489')
            data = {
                'content': form.cleaned_data['title'],
                'description': '<bc-attachment sgid="{}"></bc-attachment>'.format(
                    file_attachment_id),
            }

            specific_headers = headers.copy()
            specific_headers['Content-Type'] = 'application/json'
            requests.post(url, json=data, headers=specific_headers)
    else:
        form = forms.AudioFileForm()
    return render(request, 'audio_file_template.html', {'form': form})
