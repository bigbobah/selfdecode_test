from django import forms


class AudioFileForm(forms.Form):
    title = forms.CharField()
    file = forms.FileField()
